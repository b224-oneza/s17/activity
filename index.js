// console.log("Hello Bajun!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	
	function printUsersInfo() {
		let fullName = prompt("Enter your Full Name here"); 
		let yourAge = prompt("Enter age here"); 
		let yourLocation = prompt("Enter your location here");

		console.log("Hi, " + fullName);
		console.log("You are " + yourAge + " years old.");
		console.log("You live in " + yourLocation);
	};

	printUsersInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
		function printFavBand() {
			let bandNo1 = "IV of Spades";
			let bandNo2 = "Paramore";
			let bandNo3 = "Parokya ni Edgar";
			let bandNo4 = "Kamikazee";
			let bandNo5 = "Franco";

			console.log("1. "+ bandNo1);
			console.log("2. "+ bandNo2);
			console.log("3. "+ bandNo3);
			console.log("4. "+ bandNo4);
			console.log("5. "+ bandNo5);
		};

		printFavBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function printFavMoviesWithRatings() {
			let movieNo1 = "The Hunger Games: Catching Fire";
			let movieNo2 = "The Hunger Games: Mockingjay, Part 2";
			let movieNo3 = "Money Heist";
			let movieNo4 = "Rush Hour 3";
			let movieNo5 = "Orphan";
			let ratingFor1 = 90 + "%";
			let ratingFor2 = 70 + "%";
			let ratingFor3 = 94 + "%";
			let ratingFor4 = 17 + "%";
			let ratingFor5 = 58 + "%";

			console.log("1. "+ movieNo1);
			console.log("Rotten Tomatoes Rating: "+ ratingFor1);

			console.log("2. "+ movieNo2);
			console.log("Rotten Tomatoes Rating: "+ ratingFor2);

			console.log("3. "+ movieNo3);
			console.log("Rotten Tomatoes Rating: "+ ratingFor3);

			console.log("4. "+ movieNo4);
			console.log("Rotten Tomatoes Rating: "+ ratingFor4);

			console.log("5. "+ movieNo5);
			console.log("Rotten Tomatoes Rating: "+ ratingFor5);
			
		}

		printFavMoviesWithRatings();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
// console.log(friend3);